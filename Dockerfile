FROM java:8
COPY ./target/tm-config.jar /opt/tm-config.jar
WORKDIR /opt

EXPOSE 9090
ENTRYPOINT ["java", "-jar", "tm-config.jar"]